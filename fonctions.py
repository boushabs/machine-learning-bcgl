# -*- coding: utf-8 -*-
"""
Created in 2019
@author: BELLET ALEXANDRE
Guideline followed PEP-0008
"""

# Standard library imports
import numpy as np

from sklearn.metrics import confusion_matrix

from sklearn.svm import SVC

from sklearn.discriminant_analysis import LinearDiscriminantAnalysis

from sklearn.neural_network import MLPClassifier

from sklearn.model_selection import cross_val_score
# Local library
import utils_P as utils

import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import Axes3D
from sklearn.decomposition import PCA


# Local library
#import utils_Nov_2019 as utils
#import utils_P as utils

# Partie 1

def ACP_ALD(data, label,col):
    """ Visualiser les données via l'ACP et l'AD
    Entrées : les caractéristiques, les labels, la colonne des labels (col= 0 pour labels = 0/1 ou col=1 pour labels 2 à 8) """

    ################
    #Realiser l'acp
    acp=PCA()
    
    # remplir 
    # Calcul des coord_ldaonnées factorielles
    coord = acp.fit_transform(data)
    #...Afficher variance et boxplot
    plt.figure()
    plt.title("ACP: boxplot et variance")
    plt.subplot(2, 1, 1)
    plt.ylabel("boxplot")
    plt.xlabel("VP")
    plt.boxplot(coord[0:,0:len(coord[0])])
    #le boxplot trace la valeur médiane et la boite représente en bas le premier quartile et en haut le dernier,
    #la barre représente les limites des valeurs (range)
  
# Proportion de variance expliquée pour chaque vecteur
    plt.subplot(2, 1, 2)
    plt.stem(np.arange(1,acp.n_components_+1),np.cumsum(acp.explained_variance_ratio_))
    plt.ylabel("variance cumulee")
    plt.xlabel("nombre feature")
    
    ################
    # Visualiser l'ACP
    labels = label[:,col]
  
     # Affichage des données projetés sur VP1, VP2     
    if col==1 :
            plt.figure()
            plt.scatter(coord[labels==2,0],coord[labels==2,1],c='blue')
            plt.scatter(coord[labels==3,0],coord[labels==3,1],c='red')
            plt.scatter(coord[labels==4,0],coord[labels==4,1],c='green')
            plt.scatter(coord[labels==5,0],coord[labels==5,1],c='yellow')
            plt.scatter(coord[labels==6,0],coord[labels==6,1],c='black')
            plt.scatter(coord[labels==7,0],coord[labels==7,1],c='cyan')
            plt.scatter(coord[labels==8,0],coord[labels==8,1],c='navy') 
    else:
            plt.figure()
            plt.scatter(coord[labels==0,0],coord[labels==0,1],c='blue',marker='o')
            plt.scatter(coord[labels==1,0],coord[labels==1,1],c='red',marker='o')
 
    #############
            # Réaliser l'AD

    lda=LinearDiscriminantAnalysis()
 
    
        #Affichage sur les 2 premiers vecteurs, soit pour 2 classes (col=1) soit 8 classes (col=2)

  
     # Affichage des données projetés sur VP1, VP2     
    if col==1 :
            coord_lda = lda.fit_transform(data,labels)
            plt.figure()
            plt.scatter(coord_lda[labels==2,0],coord_lda[labels==2,1],c='blue')
            plt.scatter(coord_lda[labels==3,0],coord_lda[labels==3,1],c='red')
            plt.scatter(coord_lda[labels==4,0],coord_lda[labels==4,1],c='green')
            plt.scatter(coord_lda[labels==5,0],coord_lda[labels==5,1],c='yellow')
            plt.scatter(coord_lda[labels==6,0],coord_lda[labels==6,1],c='black')
            plt.scatter(coord_lda[labels==7,0],coord_lda[labels==7,1],c='cyan')
            plt.scatter(coord_lda[labels==8,0],coord_lda[labels==8,1],c='navy') 
    else:
            coord_lda = lda.fit_transform(data,labels)
            plt.figure()
            plt.scatter(coord_lda[labels==0,0],coord_lda[labels==0,0],c='blue',marker='o',s=10)
            plt.scatter(coord_lda[labels==1,0],coord_lda[labels==1,0],c='red',marker='x')
            
            
def matrice_confusion(expert,predict):
    """ Définition : Calcul de la matrice de confusion 
        Entrée : Tableau expert, Tableau prédit
        Sortie : Matrice de confusion, Pourcentage de bien classés, Pourcentage de bien classés par classe, 
    """
    mc = confusion_matrix(expert,predict)
    mcScore = (np.trace(mc) * 100)/len(expert) 
    c=0
    Score_classe=np.zeros(len(mc))
    for lab in range(0,len(mc)):
        Score_classe[c]=100*mc[c,c]/np.sum(mc[c,:])
        c=c+1
        
    return mc,mcScore, Score_classe
   
    

def SVM(dataTrain,labelsTrain,dataTest,fctKernel, c, deg, strategie):

    """ Définition : Crée un classifieur SVM, effectue l'apprentissage,
                     la validation croisée sur la base d'apprentisage', la validation.
        Entrée : Base d'apprentissages, Labels base d'apprentissage,
                 Base de validation, Choix du noyau, choix de C, choix du degré du polynome quand 'poly', choix stratégie classes multiples                  
        Sortie : Score base d'apprentissage en validation croisee, Classes prédites base de validation, nombre de points supports
    """

    # Création du classifieur SVM
    svc = SVC(C=c, kernel=fctKernel,gamma='scale',degree=deg, decision_function_shape=strategie)
  
    # On entraine notre classifieur avec les vecteur feature d'une BDD d'apprentissage
    # et leurs classes correspondantes   
    svc.fit(dataTrain,labelsTrain) 
    
    # On estime les classes d'une BDD de test
    labels_predit = svc.predict(dataTest)
    # on conserve les noyeaux sélectionnés
    noyeau=svc.n_support_
    
    
    # On estime le score sur la base d'apperntissage séparé en 5
    #scores = cross_val_score(svc, dataTrain, labelsTrain, cv=5)
    scores = cross_val_score(svc, dataTrain, labelsTrain, cv=5)

    return scores.mean()*100, labels_predit, noyeau
    
def MLP(dataTrain,labelsTrain,dataTest,nombre_neuronne,f_activation):   
    """ Définition : Crée un classifieur MLP, effectue l'apprentissage,
                     la validation croisée, le test. 
        Entrée : Base d'apprentissages, Labels base d'apprentissage,
                 Base de validation, Nombre de neurones couche cachée, fonction d'activation'
        Sortie : Score en apprentissage validation croisee, Classes prédites, nombre de couches 
    """
    print("Start MLP")
    utils.start_time(4)
    
    #Création du classifieur MLP, hidden_layer_size représente l'architecture du réseau
    mlp = MLPClassifier(max_iter=2000,hidden_layer_sizes=(nombre_neuronne, ),activation=f_activation, shuffle=False,random_state=1)
    #On entraine notre classifieur avec les vecteur feature d'une BDD d'apprentissage
    #et leurs classes correspondantes   
    mlp.fit(dataTrain,labelsTrain) 
    #on estime les classes d'une BDD de test
    labels_predit = mlp.predict(dataTest)    
    # On estime le score sur la base d'apperntissage séparé en 5
    scores = cross_val_score(mlp, dataTrain, labelsTrain, cv=5)
    
    print("MLP terminé en: " + utils.stop_time(4) + " secondes")
    nb_couche=mlp.n_layers_
    return scores.mean()*100, labels_predit , nb_couche
