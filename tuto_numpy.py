# -*- coding: utf-8 -*-
"""
Created on Fri May 25 12:35:02 2018

@author: ladretp

Pour comprendre Numpy
Très simple pour ceux qui viennent du matlab, beaucoup de fonctions sont similaires
Par contre l'appel de la méthode de l'objet np importé, est plus proche du C++ ou java, 
proche de la programmation objet.
"""
import numpy as np  # notre librairie numpy est appelée ici np
import matplotlib.pyplot as plt

"""Création d'une fonction, elle peut être définie à n'importe quel endroit 
du script, mais c'est plus 'propre'en début.."""

def infos(x):
    print("le nombre de dimension : ",x.ndim)
    print("le nombre total de valeurs:",x.size)
    print("la forme du tableau : ", x.shape)
    print("le type des données du tableau : ", x.dtype)
    if x.ndim > 1:
        return 1
    else:
        return 0
"""Les blocs de programmations fonctions, if, boucle ... n'ont pas de parenthèses,
 tout repose sur l'indentation après un :
Fin de la fonction , ici pour l'exemple on affecte une valeur de retour, 
mais on peut ne rien affecter"""

#_____________________________________________________________________________
# Debut programme principal

tab0=[] #initialisation basique
tab=[[1,2,3],[4,5,6]] #creation d'un tableau python compris en list voir explorateur de variables
tab2={1,2,3,4} #attention ici on a créé un ensemble (set), ce n'est pas une variable

a0=np.asarray(tab)

"""ici on crée directement un tableau numpy en entrant les valeurs et la forme 
du tableau, ici 2*2, voir la fonction np.array()"""
a = np.array( [ [1,2], [3,4] ], dtype=complex )  #le type complex est défini en python


print(a)

""" Utilisation de la fonction np.arange() il existe aussi np.linspace()
Creation d'un tableau de 20 valeurs commençant à 10 par pas de 2.
Notez que la valeur de fin 50  n'existe pas, 50 est la première valeur 
non prise dans le tableau, les valeurs vont de 10 à 48 par pas de 2"""

b=np.arange(10,50, 2, dtype='uint8')

c=np.ones((5,5),dtype='float') #creation d'un tableau 2D initialisé à 1

c2=np.arange(0,10,dtype='float').reshape((2,5)) #tableau de taille 2*5 avec 
#des valeurs allant de 0 à 9 par pas de 1

#regardez aussi l'explorateur de variables

d=infos(a)
e=infos(b)
f=infos(c)

#les opérations de manipulations de tableaux baées sur le slicing de python

val=c2[1,3] #récupération de la valeur positionnée à la ligne 1 colonne 3
t=c2[:,1] #récupération de la colonne 1, voir explorateur de variables
print("longueur de t : ",len(t))
print("\n")
t2=c2[0,:] #récupération colonne 0
t3=c2[1,1:3] #récupération sur la ligne 1 des valeurs comprises 
#depuis la colonne 1 jusqu'à la colonne 3 non comprise!
val2=t[-1] #indice négatif permet de parcourir le tableau depuis la fin (-1) 

c3=c2.reshape((5,2)) #mettre les données sous un autre format
c4=np.transpose(c2) #faire la transposition de c2 ce qui est différent de c3

print("\n")
infos(c4)
#____________________________________________________________________________
#exemple avec des lettres
alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

# Les 5 premiers
print(alpha[:5])

# Les 5 derniers
print(alpha[-5:])

# Tous sauf les 5 premiers
print(alpha[5:])

# Tous sauf les 5 derniers
print(alpha[:-5])

#_____________________________________________________________________________
#les librairies de numpy
x2=np.linspace(0,0.1,1000) #creation d'un axe des temps de 0 à 0.1s sur 1000 échantillons
#attention le pas n'est pas de 1/1000, car 0.1 est ici inclus, 
#donc si on veut exactement un pas de 1/1000 entre chaque échantillon le plus simple :
pas=1/1000
x=np.arange(0,0.1,pas)

f1=50
y1=np.sin(2*np.pi*f1*x) #y1 résultat pour chaque x de sin(2pifx)
#Les fonctions mathématiques s'appliquent sur chaque élément du tableau numpy
#usage de la librairie d'affichage matplotlib.pyplot une quasi copie de la librairie matlab
print("\nregardez les figures qui viennent d'être créee! cliquez sur les Logo spyder\n")
plt.figure(1)
plt.plot(x,y1), plt.title("sinus")
plt.xlabel('temps en seconde')

f2=20
y2=np.cos(2*np.pi*f2*x)
y3=y1+y2 #y3 est la somme terme à terme des deux vecteurs y1 et y2
y4=y1*y2 #y4 est la multiplication terme à terme , donc différent de la multiplication de matrice.
#C'est la grande différence d'avec matlab! 

y5=np.linalg.multi_dot([y3,y4]) # ici appel du sous package algebre linéaire et de la focntion de produit scalaire


plt.figure() #crée une nouvelle figure en plus de celles existantes
plt.subplot(2,1,1), plt.plot(x,y3), plt.title('somme de sinus'),plt.xlabel('temps en seconde'), plt.ylabel('amplitude')
plt.subplot(2,1,2), plt.plot(x,y4), plt.title('multiplication terme à terme '),plt.xlabel('temps en seconde')

###############################################################################
# Possibilité d'utiliser le sous-package matrix de numpy pour réaliser de 
#la multiplication de matrice * ou exponentielle de matrice **
##############################################################################

z1=np.matrix(c3) #les arrays doivent être des tableaux 2D
z2=np.matrix(c2)

y6=z1*z2
# ici après transformation en classe matrix c'est la multiplication de matrice qui est réalisée!



