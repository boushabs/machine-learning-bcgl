# Machine Learning - BCGL
### Classification automatique supervisée d’images naturelles et artificielles

L’objectif est de concevoir un système capable de classer automatiquement des images en
différentes catégories.  
On dispose pour cela d’une banque de 2288 images numérotées. Chaque
image possède 2 catégories principales et chaque catégorie possède plusieurs sous-catégories.
  
La première catégorie représente la classe principale de l’image, elle peut être :  
&nbsp;&nbsp;&nbsp;&nbsp;- Artificielle (constructions humaines)  
&nbsp;&nbsp;&nbsp;&nbsp;- Naturelle (image de nature)

La seconde catégorie représente la sous-classe de l’image, il y en a 8 :  
&nbsp;&nbsp;&nbsp;&nbsp;- Côte (plage, mer…)  
&nbsp;&nbsp;&nbsp;&nbsp;- Forêt  
&nbsp;&nbsp;&nbsp;&nbsp;- Autoroute  
&nbsp;&nbsp;&nbsp;&nbsp;- Ville (Photo prise de loin de l’ensemble de la ville)  
&nbsp;&nbsp;&nbsp;&nbsp;- Montagne  
&nbsp;&nbsp;&nbsp;&nbsp;- Paysage ouvert (landes, désert…)  
&nbsp;&nbsp;&nbsp;&nbsp;- Rue  
&nbsp;&nbsp;&nbsp;&nbsp;- Grand bâtiment

Chaque image est décrite à l’aide de 27 vecteurs de caractéristiques (features en
anglais). Les 24 premiers vecteurs de caractéristiques sont issus d’un banc de filtres de Gabor
et consistent à récupérer l’énergie dans chacune des bandes de fréquences (ici 24). Les 3
derniers vecteurs de caractéristiques sont issus de la luminance Y et de la chrominance Cb puis
Cr.  

Un programme en python intitulé main.py vous est fourni. Il permet de convertir la base de
données, initialement en format csv, en plusieurs matrices composées des caractéristiques et
des labels des images.  
datatrain, datavalidation sont les matrices contenant les 27 caractéristiques, séparées en une
base pour l’apprentissage et une base pour la validation.  
datalabeltrain, datalabelvalidation sont des matrices de même taille que datatrain,
datavalidation. Elles sont composées de 2 colonnes, la première portant le label 0 (Artificielle)
ou 1 (Naturelle), la deuxième portant un label parmi 2 à 8 pour les catégories d’images,
respectivement côte, forêt ....  
Pour information, les images sont accessibles dans le fichier images.zip et peuvent être
visualisées.  

L’objectif de ce projet est de proposer un classifieur parmi un SVM et un MLP qui permette
de répondre le mieux au problème de classification posé (classification images
naturelles/artificielles, classification par catégories d’images).  
Pour vous aider, vous disposez d’un fichier nommé fonctions.py qui contient les fonctions dont
vous avez besoin pour réaliser le projet. Il contient :  
&nbsp;&nbsp;&nbsp;&nbsp;- une fonction permettant de réaliser une ACP et une analyse discriminante sur la base 
d’apprentissage et de projeter les points dans le nouvel espace de représentation  
&nbsp;&nbsp;&nbsp;&nbsp;- une fonction permettant de calculer une matrice de confusion  
&nbsp;&nbsp;&nbsp;&nbsp;- une fonction permettant d’entrainer un SVM  
&nbsp;&nbsp;&nbsp;&nbsp;- une fonction permettant d’entrainer un MLP.  

Afin de bien comprendre ce que réalise chaque fonction, il est nécessaire d’étudier sur le site
de sikitlearn la documentation relative aux fonctions importées et notamment MLPClassifier
et svc. Pour ces fonctions, il est nécessaire de prendre le temps de comprendre les paramètres
d’entrées et les attributs associés à chaque fonction.  

https://scikit-learn.org/stable/modules/neural_networks_supervised.html#multi-layerperceptron  
https://scikit-learn.org/stable/modules/generated/sklearn.neural_network.MLPClassifier.html

https://scikit-learn.org/stable/modules/svm.html  
https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html

https://scikit-learn.org/stable/modules/cross_validation.html  
https://scikitlearn.org/stable/modules/generated/sklearn.model_selection.cross_val_score.html

Pour chaque classifieur, on choisira les paramètres de réglage qui permettent d’obtenir les
meilleures performances sur la base de test (base d’apprentissage, en validation croisée, voir
fonction cross_val_score). On comparera les performances des classifieurs SVM et MLP
optimisés sur la base de validation. On prendra soin de calculer les intervalles de confiance des
scores.  
Vous pourrez évaluer la sensibilité des résultats à la base d’apprentissage, à la taille de la base
d’apprentissage. Vous pourrez aussi évaluer la perte en performances quand on supprime les 3
dernières caractéristiques, c’est-à-dire la luminance Y, la chrominance Cb et Cr.


