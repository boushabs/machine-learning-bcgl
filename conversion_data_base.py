# -*- coding: utf-8 -*-
"""
Created on Thu Oct 15 10:06:55 2020

@author: charbons
"""


# -*- coding: utf-8 -*-
"""
Created on Tue Dec 17 15:10:54 2019

@author: ladretp
"""

# Standard library imports
import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt

# Local library
import utils_P as utils

#import correction_Partie1_P as partie1
#import correction_Partie2_P as partie2

def mon_main1et2(RangeApprentissage,ColLabels,shake):
    if shake==1:
        utils.shake_database('./database.csv')
    database = pd.read_csv('./database.csv')
# Paramètres
# Nombre d'images dans la base d'apprentissage 70% de la base de données
#RangeApprentissage = 400 #nombre petit => nbr images en test important
# Etude sur catégorie 2 classes == 0 ou 8 classes == 1

# Partie 2.1.1 et Partie 2.1.2
## Construction de la base d'apprentissage et de test
    datatrain = database.iloc[0:RangeApprentissage-1,3:]
    datalabeltrain = database.iloc[0:RangeApprentissage-1,1:3]
    datatest = database.iloc[RangeApprentissage:,3:]
    datalabeltest = database.iloc[RangeApprentissage:,1:3]
# Partie 2.1.3
# Affichage des données contenus dans la base d'apprentissage
    database_train = pd.concat((datalabeltrain,datatrain),axis=1)
    print("Base d'apprentissage:")
    utils.show_database(database_train,0,1) 
    database_test = pd.concat((datalabeltest,datatest),axis=1)
    Test1,Test2=utils.show_database(database_test,0,1) 

## Conversion string en int
    datalabeltrain = utils.unset_labels(datalabeltrain,0,1)
    datalabeltest = utils.unset_labels(datalabeltest,0,1)
    datalabeltrain = datalabeltrain.astype(int)
    datalabeltest = datalabeltest.astype(int)

    truelabel=datalabeltest.iloc[:,ColLabels]
    datatrain=np.array(datatrain)
    datatest=np.array(datatest)
    datalabeltrain=np.array(datalabeltrain)
    datalabeltest=np.array(datalabeltest)
    
    return datalabeltrain, datalabeltest, datatrain, datatest, truelabel




